import java.util.ArrayList;

import java.util.Date;
import java.util.List;



public class CartItem implements IcartItem {
	
	public int ID ;
	public int productID ;
	public int Quantity ;
	public double UnitPrice ;
	public double TotalCost ;
	
	
	public int get_ID (){
		return ID ;	
	}
	
	public int get_productID (){
		return productID ;
	}
	
	public int get_Quantity () {
		return Quantity ;
	}
	public double get_UnitPrice (){
		return UnitPrice ;
	}
	
	public double get_TotalCost (){
		return TotalCost ;
	}

	public void set_ID (int id){
		ID=id ;
	}
	
	public void set_productID (int id){
		productID=id ;
	}
	
	public void set_Quantity (int id){
		Quantity=id ;
	}
	
	public void set_UnitPrice (double price){
		UnitPrice=price ;
	}
	
	
	
	public CartItem (int id ,int productid , int quantity , double price){}


}
