import java.util.ArrayList;

import java.util.Date;
import java.util.List;

public class ShoppingCart implements IShoppingCart { 
	 
	
	    int id;
	    int sessionid;
	    int customerid;
	    ArrayList<IcartItem> itemsArray = new ArrayList<IcartItem>();;
	    Date lastAccessed ;
		
	    
	    public int getId()
	    {
	        return id;
	    }
	    public int getSessionID()
	    {
	        return sessionid;
	    }
	    public int getCustomerID()
	    {
	        return customerid;
	    }
	    
	  public void addItem(IcartItem item)
	    {
		   
		    //item.getID();
		   // item.getproductID() ;
		    //item.getQuantity();
		    //item.getUnitPrice() ;
 
	        itemsArray.add(item) ;
	    }
	   
	    
	    public void updateQuantity (int cartItemID , int newQuantity)
	    {
	    	for (int i=0 ; i< itemsArray.size() ;i++ ){
	    		if (cartItemID== itemsArray.get(i).getID()){
	    			
	    			itemsArray.get(i).setQuantity(newQuantity);
	    			return ;
	    		}
	    		
	    	}
	    	
	    	
	        
	    }
	    public void removeItem(int cartItemId)
	    {
	    	boolean  re=false ;
	    	for (int i=0 ; i< itemsArray.size() ;i++ ){
	    		if (cartItemId== itemsArray.get(i).getID() ){
	    			itemsArray.remove(i) ;
	    			re=true ;
	    			return ;
	    		}
	    		else 
	    			re = false ;
	    		 
	    			
	    	}
	        
	    }
	    public IcartItem getItem(int productID)
	    {
	    	IcartItem item = new CartItem(0,0,0,0) ;
	    	int x,y,z ;
	    	double w ;
			for (int i=0 ; i<itemsArray.size() ; i++){
				if (productID==itemsArray.get(i).getproductID()){
					 x= itemsArray.get(i).getID() ;
					 y= itemsArray.get(i).getproductID() ;
					 z= itemsArray.get(i).getQuantity() ;
				     w =itemsArray.get(i).getUnitPrice() ;
				        item.setID(x);
						item.setproductID(y);
						item.setQuantity(z);
						item.setUnitPrice(w);
						
				}	
				
			}
			
			return item  ; 
	    }
	    
	    public ArrayList<IcartItem> getItems ()
	    {
	    	ArrayList<IcartItem> items = new ArrayList<IcartItem>();
	        for (int i= 0 ; i< itemsArray.size() ; i++ ){
	        	itemsArray.get(i).getID();
	        	itemsArray.get(i).getproductID();
	        	itemsArray.get(i).getQuantity();
	        	itemsArray.get(i).getUnitPrice() ;
	        	items.add(itemsArray.get(i)) ;
	        	
	        }
	        	
	        return  items ;
	    }
	    
	    public int countItems()
	    {
	    	int x=	itemsArray.size() ;
          	 return x ; 
	    }
	    public Date getLastAccessedDate ()
	    {
	       
			return lastAccessed;
	    }
	    public ShoppingCart(int cartID , int customerID , int sessionID)
	    {
	        id=cartID;
	        customerid=customerID;
	        sessionid= sessionID;
	    }
	    

}
