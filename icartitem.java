import java.util.ArrayList;

import java.util.Date;
import java.util.List;

public interface IcartItem {
	public int get_ID ();
	
	public int get_productID ();
	
	public int get_Quantity ();
	public double get_UnitPrice ();
	
	public double get_TotalCost ();

	public void set_ID (int id);
	
	public void set_productID (int id);
	
	public void set_Quantity (int id);
	
	public void set_UnitPrice (double price);
	